/* Trainer Class Constructor*/
class Trainer {
    constructor(name, age, pokemon, friends) {
        this.name = name;
        this.age = age;
        this.pokemon = pokemon;
        this.friends = friends;
    }
}

/* Pokemon Class Constructor*/
class Pokemon {
    constructor(name, level) {
        this.name = name;
        this.level = level;
        this.health = level * 100;
        this.attack = level * 10;
    }
}

/* Created Ash Kechum Trainer */
let ashKechum = new Trainer(`Ash Kechum`, 10, [`Pikachu`, `Charizard`, `Squirtle`, `Bulbasaur`], {hoenn: [`May`, `Max`], kanto: [`Brock`, `Misty`]});

/* Trainer Talk when choosing pokemon*/
Trainer.prototype.talk = choosePokemon => {
    console.log(`${choosePokemon.name}! I choose you!`);
}

/* Ash Kechum Pokemons */
ashKechum.pokemon[0] = new Pokemon(`Pikachu`, 99);
let pikachu = ashKechum.pokemon[0];
ashKechum.pokemon[1] = new Pokemon(`Charizard`, 99);
let charizard = ashKechum.pokemon[1];
ashKechum.pokemon[2] = new Pokemon(`Squirtle`, 99);
let squirtle = ashKechum.pokemon[2];
ashKechum.pokemon[3] = new Pokemon(`Bulbasaur`, 99);
let bulbasaur = ashKechum.pokemon[3];

/* Pokemon Tackle Skill and Faint */
Pokemon.prototype.tackle = (attacker, targetEnemy) => {
    targetEnemy.health -= attacker.attack;
    console.log(`${attacker.name} tackled ${targetEnemy.name}!`)
    console.log(`${targetEnemy.name}'s health is now reduced to ${targetEnemy.health}.`);
    if(targetEnemy.health <= 0){
        targetEnemy.faint(targetEnemy);
    }
    console.log(targetEnemy);
}
Pokemon.prototype.faint = (targetEnemy) => {
    console.log(`${targetEnemy.name} has fainted.`);
    }

/* Added Critical Tackle */
Pokemon.prototype.tackleCritical = (attacker, targetEnemy) => {
    targetEnemy.health -= (attacker.attack * 10);
    console.log(`${attacker.name} critically tackled ${targetEnemy.name}.`)
    console.log(`${targetEnemy.name}'s health is now reduced to ${targetEnemy.health}.`);
    if(targetEnemy.health <= 0){
        targetEnemy.faint(targetEnemy);
    }
    console.log(targetEnemy);
}

/* Pokemon Level Up */
Pokemon.prototype.levelUp = (pokemon, levelAdded) => {
    pokemon.level += levelAdded;
    pokemon.health += (levelAdded*100);
    pokemon.attack += (levelAdded*10);
    console.log(`${pokemon.name} level up!`)
    console.log(`${pokemon.name} is now level ${pokemon.level}!`)
    console.log(pokemon);
}

/* Enemy Pokemon */
let geodude = new Pokemon(`Geodude`, 5);
let mewTwo = new Pokemon(`Mewtwo`, 99);

/* Test Log */
console.log(ashKechum);
console.log(`Result of dot notation`);
console.log(ashKechum.name);
console.log(`Result of square bracket notation`);
console.log(ashKechum[`pokemon`])
console.log(`Result of talk method`);
ashKechum.talk(pikachu);
console.log(pikachu);
console.log(geodude);
console.log(mewTwo);

/* Battle Starts */

geodude.tackle(geodude, pikachu);

mewTwo.tackle(mewTwo, geodude);

/* Battle Continues (sorry ma'am na carried away lang.,ehehe) */

mewTwo.tackleCritical(mewTwo, pikachu);

ashKechum.talk(squirtle);

squirtle.tackle(squirtle, mewTwo);

mewTwo.tackleCritical(mewTwo, squirtle);

ashKechum.talk(bulbasaur);

bulbasaur.tackle(bulbasaur, mewTwo);

mewTwo.tackleCritical(mewTwo, bulbasaur);

ashKechum.talk(charizard);

charizard.tackleCritical(charizard, mewTwo);

/* Batlle Ends Ash's Pokemons Leveled Up */
charizard.levelUp(charizard, 5);
pikachu.levelUp(pikachu, 3);
squirtle.levelUp(squirtle, 3);
bulbasaur.levelUp(bulbasaur, 3);

/* Note - nitry ko po gamitin ung this. sa function pero ayaw po gumana kya paulit ulit ung name nila sa paramaters baka alam nyo po kung panu ayusin. */